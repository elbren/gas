/********************************************************************
# Copyright 2021 Daniel 'grindhold' Brendle
#
# This file is part of gas.
#
# gas is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# gas is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with gas.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Gas {
    public class Server {
        /**
         * The size of the buffer that transmits the raw websocket packets
         * Setting this too low can produce weird behaviour on the client.
         * E.g. surfaces that only appear when you hover over them.
         */
        public const int WS_PROXY_BUFFER_SIZE = 1024*1024;

        /**
         * Maximum size of the eventlist given by epoll
         */
        public const int EVENTSIZE = 64;

        private HashTable<string, int> c2s = new HashTable<string, int>(str_hash, str_equal);
        private HashTable<string, int> s2c = new HashTable<string, int>(str_hash, str_equal);

        public static Authenticator authenticator = new MockAuthenticator();

        private List<Socket> sockets = new List<Socket>();
        private int epoll_ctx = -1;
        private Socket? listener_v4 = null;
        private Socket? listener_v6 = null;

        public GLib.Socket? create_socket(string address, bool v6) {
            InetAddress inetaddress = new InetAddress.from_string (address);

            int port = 8080;

            try {
                port = Config.c.get_integer("Server", "Port");
            } catch (GLib.KeyFileError e) {
                warning("Could not read 'Port' from config. Defaulting to port %d", port);
            }

            if (port < 0 || port > 65535) {
                warning("Port must be between 0 and 65535");
                return null;
            }

            message("Spawning socket on %s:%d", address, port);
            InetSocketAddress socketaddress = new InetSocketAddress (inetaddress, (uint16)port);

            GLib.Socket? socket;
            try {
                if (v6) {
                    socket = new Socket (SocketFamily.IPV6, SocketType.STREAM, SocketProtocol.TCP);
                } else {
                    socket = new Socket (SocketFamily.IPV4, SocketType.STREAM, SocketProtocol.TCP);
                }
            } catch (Error e) {
                warning("Could not spawn listener socket for %s".printf(address));
                return null;
            }
            assert (socket != null);

            try {
                socket.bind (socketaddress, true);
                socket.set_listen_backlog (10);
                socket.blocking = false;
                socket.listen ();
            } catch (Error e) {
                warning("Could not set up socket on %s:".printf(address) + e.message);
                return null;
            }
            return socket;
        }

        public void init_authenticator() {
            var authmode = "";
            try {
                authmode = Config.c.get_string("Server", "Authenticator");
            } catch (GLib.KeyFileError e) {
                critical("Please configure the 'Authenticator' field in gas.ini");
            }
            switch(authmode) {
                case "Mock":
                    Server.authenticator = new MockAuthenticator();
                    break;
                case "LDAP":
                    Server.authenticator = new LdapAuthenticator();
                    break;
                default:
                    critical("%s is not a valid authenticator", authmode);
                    break;
            }
        }

        public Server () {
            // Initialize authentication
            this.init_authenticator();
            // Initialize networking
            this.epoll_ctx = Linux.epoll_create1(0);
            var event = Linux.EpollEvent();

            
            event.events = Linux.EPOLLIN | Linux.EPOLLET;
            try {
                this.listener_v4 = this.create_socket(
                    Config.c.get_string("Server", "IPv4"), false
                );
                event.data.fd = this.listener_v4.fd;
                Linux.epoll_ctl(epoll_ctx, Linux.EPOLL_CTL_ADD, this.listener_v4.fd, event);
            } catch (GLib.KeyFileError e) {
                warning ("Skipping IPv4 due to configuration problem");
            }

            try {
                this.listener_v6 = this.create_socket(
                    Config.c.get_string("Server", "IPv6"), true
                );
                event.data.fd = this.listener_v6.fd;
                Linux.epoll_ctl(epoll_ctx, Linux.EPOLL_CTL_ADD, this.listener_v6.fd, event);
            } catch (GLib.KeyFileError e) {
                warning ("Skipping IPv6 due to configuration problem");
            }

            if (this.listener_v4 == null && this.listener_v6 == null) {
                critical("Could not start one socket");
                return;
            }


            try {
                this.serve();
            } catch (Error e) {
                critical("An error occurred during serving: ");
            }
        }

        public void register_client_socket(Socket connection) {
            connection.blocking = false;
            //connection.keepalive = true;
            var event = Linux.EpollEvent();
            event.data.fd = connection.fd;
            event.events = Linux.EPOLLIN | Linux.EPOLLET;
            Linux.epoll_ctl(epoll_ctx, Linux.EPOLL_CTL_ADD, connection.fd, event);
            sockets.append(connection);
        }

        public void register_server_socket(Socket server_connection, Socket client_connection) {
            server_connection.blocking = false;
            //server_connection.keepalive = true;
            var event = Linux.EpollEvent();
            event.data.fd = server_connection.fd;
            event.events = Linux.EPOLLIN | Linux.EPOLLET;
            Linux.epoll_ctl(epoll_ctx, Linux.EPOLL_CTL_ADD, server_connection.fd, event);
            sockets.append(server_connection);
            this.c2s.insert("%i".printf(client_connection.fd), server_connection.fd);
            this.s2c.insert("%i".printf(server_connection.fd), client_connection.fd);
        }

        private void serve () throws Error {
            var events = new Linux.EpollEvent[Server.EVENTSIZE];

            while(true) {
                var n = Linux.epoll_wait(epoll_ctx, events, -1);
                for (int i = 0 ; i < n ; i++) {
                    if ((events[i].events & Linux.EPOLLERR) > 0 || 
                        (events[i].events & Linux.EPOLLHUP) > 0 ||
                        !(( events[i].events & Linux.EPOLLIN ) > 0)) {
                        var connection = new Socket.from_fd(events[i].data.fd);
                        connection.close();
                        continue;
                    }
                    if (this.listener_v4 != null && events[i].data.fd == this.listener_v4.fd) {
                        Socket connection = this.listener_v4.accept ();
                        this.register_client_socket(connection);
                    } else if (this.listener_v6 != null && events[i].data.fd == this.listener_v6.fd) {
                        Socket connection = this.listener_v6.accept ();
                        this.register_client_socket(connection);
                    } else {
                        var connection = new Socket.from_fd(events[i].data.fd);
                        int socketfd = events[i].data.fd;
                        var keep_socket = false;
                        if (this.s2c.contains("%i".printf(socketfd))) {
                            Socket client_connection = new Socket.from_fd(this.s2c.get("%i".printf(socketfd)));
                            ssize_t received = 0;
                            uint8[] buf = new uint8[Server.WS_PROXY_BUFFER_SIZE];
                            received = connection.receive(buf);
                            buf.length = (int)received;
                            try {
                                client_connection.send(buf);
                            } catch (GLib.Error e) {
                                warning("Could not send buffer to %i", socketfd);
                            }
                            keep_socket = true;
                            sockets.append(client_connection);
                        } else if (this.c2s.contains("%i".printf(socketfd))) {
                            Socket server_connection = new Socket.from_fd(this.c2s.get("%i".printf(socketfd)));
                            ssize_t received = 0;
                            uint8[] buf = new uint8[Server.WS_PROXY_BUFFER_SIZE];
                            received = connection.receive(buf);
                            buf.length = (int)received;
                            try {
                                server_connection.send(buf);
                            } catch (GLib.Error e) {
                                warning("Could not send buffer to %i", socketfd);
                            }
                            keep_socket = true;
                            sockets.append(server_connection);
                        } else {
                            keep_socket = Request.process(this, connection);
                        }

                        if (keep_socket) {
                            sockets.append(connection);
                        }
                    }
                }
            }
        }
    }

    public static int main (string[] args) {
        Config.init();
        Request.init();
        new Server();
        return 0;
    }
}
