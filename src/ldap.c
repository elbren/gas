/********************************************************************
# Copyright 2021 Daniel 'grindhold' Brendle
#
# This file is part of gas.
#
# gas is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# gas is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with gas.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ldap.h>
#include <glib.h>
/* LDAP Server settings */

int
gas_ldap_authenticate(const char *base_dn,
                 const char *bind_user,
                 const char *bind_pw,
                 const char *auth_user,
                 const char *auth_pw,
                 const char *filtertemplate,
                 const char *server, int port)
{
    LDAP *ld;
    int rc;
    int version = 3;
    char bind_server[100];
    char bind_dn[100];


    sprintf(bind_dn, "%s,%s", bind_user, base_dn);
    sprintf(bind_server, "%s:%d", server, port);

    if (ldap_initialize(&ld, bind_server)) {
        g_warning("Could not bind to LDAP 1st stage\n");
        return 0;
    }

    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);

    /* User authentication (bind) */
    rc = ldap_simple_bind_s(ld, bind_dn, bind_pw);
    if (rc != LDAP_SUCCESS) {
        g_warning("Could not bind to LDAP with bind user\n");
        ldap_unbind(ld);
        return 0;
    }

    char* filter[100];
    sprintf(filter, filtertemplate, auth_user);

    LDAPMessage* res;
    LDAPMessage* msg;
    char* searchattrs[1];
    searchattrs[0] = "cn";
    rc = ldap_search_ext_s(ld, base_dn, LDAP_SCOPE_SUBTREE,
                            filter, searchattrs, 0, NULL,NULL, NULL, -1, &res);

    char* found_cn = NULL;
    char* attr = NULL;
    char* val = NULL;
    struct berval** cnval = NULL;
    BerElement* ber;
    for (msg=ldap_first_message(ld, res); msg != NULL; msg=ldap_next_message(ld,msg)) {
        int msgtype = ldap_msgtype(msg);
        if (msgtype == LDAP_RES_SEARCH_ENTRY) {
            for (attr = ldap_first_attribute(ld, res, &ber); attr != NULL; attr= ldap_next_attribute(ld, res, ber) ) {
                if (strcmp(attr,"cn") == 0) {
                    cnval = ldap_get_values_len(ld, res, attr);
                    found_cn = (*cnval)->bv_val;
                }
            }
        }
    }
    ldap_unbind(ld);
    char* user_dn[100];
    sprintf(user_dn, "cn=%s,%s", found_cn, base_dn);

    if (ldap_initialize(&ld, bind_server)) {
        g_warning("Could not bind to LDAP 2nd stage\n");
        return 0;
    }

    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);
    rc = ldap_simple_bind_s(ld, user_dn, auth_pw);
    ldap_unbind(ld);
    if (rc != LDAP_SUCCESS) {
        return 0;
    }
    return 1;
}
