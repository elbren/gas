/********************************************************************
# Copyright 2021 Daniel 'grindhold' Brendle
#
# This file is part of gas.
#
# gas is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# gas is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with gas.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Gas {
    public enum Method {
        GET, POST, INVALID
    }

    /**
     * Holds Gas-relevant data in a http request
     */
    public class Request {
        private static uint8[] login_html;
        private static uint8[] broadway_html;
        private static uint8[] broadway_js;

        public string resource {get; private set; default="";}
        public string sid {get; private set; default="";}
        public Method method {get; private set; default=Method.INVALID;}
        public string username {get; private set; default="";}
        public string password {get; private set; default="";}
        public int status_code {get; private set; default=200;}

        public Request(uint8[] raw) {
            var data = ((string)raw).split("\r\n");
            var statusline = data[0];

            // Parse status line
            if (!(statusline.has_suffix ("HTTP/1.0") || statusline.has_suffix("HTTP/1.1"))) {
                this.status_code = 400;
                return;
            }
            statusline = statusline.replace(" HTTP/1.0","");
            statusline = statusline.replace(" HTTP/1.1","");

            if (statusline.has_prefix ("GET")) {
                statusline = statusline.replace("GET ", "");
                this.method = Method.GET;
            } else if (statusline.has_prefix ("POST")){
                statusline = statusline.replace("POST ", "");
                this.method = Method.POST;
            } else {
                this.status_code = 405;
                return;
            }
            this.resource = statusline;

            if (this.resource == "/favicon.ico") {
                this.status_code = 404;
                return;
            }

            // Parse Headers
            int i;
            for (i = 1; i < data.length; i++ ) {
                if (data[i] == "") {
                    i++;
                    break;
                }
                var headerline = data[i].split(":",2);
                var key = headerline[0].strip().down();
                if (key == "cookie") {
                    var cookie = this.get_cookie_with_key(headerline[1], "gas_auth");
                    if (cookie != null) {
                        this.sid = cookie;
                    }
                }
            }

            // Parse Body
            if (data.length == i + 1) {
                var parameters = data[i].split("&");
                foreach (var parameter in parameters) {
                    var parts = parameter.split("=",2);
                    if (parts[0].strip() == "rthgjri") {
                        this.username = parts[1];
                        continue;
                    }
                    if (parts[0].strip() == "wehieo") {
                        this.password = parts[1];
                    }
                }
            }
        }

        private string? get_cookie_with_key(string data, string key) {
            var cookies = data.split(";");
            foreach (string cookie in cookies) {
                var cookieline = cookie.split("=", 2);
                if (cookieline[0].strip() == "gas_auth") {
                    return cookieline[1].strip();
                }
            }
            return null;
        }

        /**
         * Prepare the resources that we serve directly
         */
        public static void init() {
            try {
                Request.login_html = GLib.resources_lookup_data(
                        "/de/grindhold/gas/web/login.html", 
                        GLib.ResourceLookupFlags.NONE
                ).get_data();
                Request.broadway_html = GLib.resources_lookup_data(
                        "/de/grindhold/gas/web/broadway.html", 
                        GLib.ResourceLookupFlags.NONE
                ).get_data();
                Request.broadway_js = GLib.resources_lookup_data(
                        "/de/grindhold/gas/web/broadway.js", 
                        GLib.ResourceLookupFlags.NONE
                ).get_data();
            } catch (GLib.Error e) {
                message("Couldn't load login html resource");
            }
        }

        public const int MAX_ENTITY_SIZE = 1024;

        public static bool process(Server server, Socket connection) {
            uint8[] buf = new uint8[MAX_ENTITY_SIZE + 1];
            ssize_t buf_len = 0;
            try {
                buf_len = connection.receive(buf);
            } catch (GLib.Error e) {
                warning("Could not read HTTP request from socket");
            }
            if (buf_len >= MAX_ENTITY_SIZE + 1) {
                try {
                    connection.send ("HTTP/1.1 413 Request Entity Too Large\r\n\r\n<html><head><title>413</title></head><body>Too fat.</body></html>".data);
                } catch (Error e) {
                    warning("could not send 413 to client");
                }
            }

            var req = new Request(buf);
            /*message(req.jwt);
            message(req.username);
            message(req.password);
            message("resource: "+req.resource);
            message("status %d",req.status_code);*/
            if (req.status_code != 200) {
                // Send error
            }

            if (req.resource == "/") {
                switch (req.method) {
                    case Method.GET:
                        if (req.sid == "") {
                            try {
                                connection.send ("HTTP/1.1 200 OK\r\n".data);
                                connection.send ("Content-Type: text/html\r\n".data);
                                connection.send ("Content-Encoding: utf-8\r\n".data);
                                connection.send ("\r\n".data);
                                connection.send (login_html);
                            } catch (GLib.Error e) {
                                warning("Could not send login page to socket");
                            }
                            return false;
                        } else {
                            try {
                                connection.send ("HTTP/1.1 200 OK\r\n".data);
                                connection.send ("Content-Type: text/html\r\n".data);
                                connection.send ("Content-Encoding: utf-8\r\n".data);
                                connection.send ("\r\n".data);
                                connection.send (broadway_html);
                            } catch (GLib.Error e) {
                                warning("Could not send broadway HTML to socket");
                            }
                            return false;
                        }
                    case Method.POST:
                        var username = Request.decode_url(req.username);
                        var password = Request.decode_url(req.password);
                        if (Server.authenticator.authenticate(username, password)) {
                            var session = new Session(username);
                            var sid = session.get_sid();
                            try {
                                connection.send ("HTTP/1.1 200 OK\r\n".data);
                                connection.send ("Set-Cookie: gas_auth=%s\r\n".printf(sid).data);
                                connection.send ("Content-Type: text/html\r\n".data);
                                connection.send ("Content-Encoding: utf-8\r\n".data);
                                connection.send ("\r\n".data);
                                connection.send (broadway_html);
                            } catch (GLib.Error e) {
                                warning("Could not send new session broadway HTML to socket");
                            }
                            return false;
                        } else {
                            try {
                                connection.send ("HTTP/1.1 200 OK\r\n".data);
                                connection.send ("Content-Type: text/html\r\n".data);
                                connection.send ("Content-Encoding: utf-8\r\n".data);
                                connection.send ("\r\n".data);
                                connection.send (login_html);
                            } catch (GLib.Error e) {
                                warning("Could not send retry login page to socket");
                            }
                            return false;
                        }
                    case Method.INVALID:
                        error("This should never be reached");
                }
            }
            if (req.resource == "/broadway.js") {
                try {
                    connection.send ("HTTP/1.1 200 OK\r\n".data);
                    connection.send ("Content-Type: text/javascript\r\n".data);
                    //connection.send ("Content-Encoding: utf-8\r\n".data);
                    connection.send ("\r\n".data);
                    connection.send (broadway_js);
                } catch (GLib.Error e) {
                    warning("Could not send broadway js to socket");
                }
                return false;
            }
            if (req.resource == "/socket") {
                if  (req.sid == "") {
                    try {
                        connection.send ("HTTP/1.1 401 Unauthorized\r\n\r\n".data);
                        connection.send (login_html);
                    } catch (GLib.Error e) {
                        warning("Could not send 401 to socket");
                    }
                }

                var socketaddress = "/var/run/gas/%s.sock".printf(req.sid);
                var address = new UnixSocketAddress(socketaddress);

                Socket socket = null;
                try {
                    socket = new Socket (
                        SocketFamily.UNIX, SocketType.STREAM, SocketProtocol.DEFAULT
                    );
                } catch (GLib.Error e) {
                    warning("Could not open socket to broadway backend");
                }
                if (socket == null) {
                    warning("Got null broadway socket");
                }

                try {
                    socket.connect (address);
                }  catch (GLib.Error e) {
                    warning("Could not connect to broadway socket");
                }
                print ("Connected\n");

                server.register_server_socket(socket, connection);

                buf.length = (int)buf_len;
                try {
                    socket.send(buf);
                } catch (GLib.Error e) {
                    warning("Could not forward websocket handshake to broadway");
                }
                return true;
            }
            try {
                connection.send ("HTTP/1.1 404 Not Found\r\n\r\n<html><body><p>File not Found</p></body></html>".data);
                connection.close();
            } catch (GLib.Error e) {
                warning("Could not send 404");
            }
            return false;
        }
        private static string decode_url(string input) {
            //TODO: missing values > 127
            //TODO: make a more performant 1-pass solution
            var url = input.replace("%20"," ").replace("%21","!").replace("%22","\"");
            url = url.replace("%23","#").replace("%24","$").replace("%26","&");
            url = url.replace("%27","'").replace("%28","(").replace("%29",")");
            url = url.replace("%2b","+").replace("%2B","+");
            url = url.replace("%2c",",").replace("%2C",",");
            url = url.replace("%2f","/").replace("%2F","/");
            url = url.replace("%3a",":").replace("%3A",":");
            url = url.replace("%3b",";").replace("%3B",";");
            url = url.replace("%3c","<").replace("%3C","<");
            url = url.replace("%3d","=").replace("%3D","=");
            url = url.replace("%3e",">").replace("%3E",">");
            url = url.replace("%3f","?").replace("%3F","?");
            url = url.replace("%40","@").replace("%40","@");
            url = url.replace("%5b","[").replace("%5B","[");
            url = url.replace("%5c","\\").replace("%5C","\\");
            url = url.replace("%5d","]").replace("%5d","]");
            url = url.replace("%60","`").replace("%60","`");
            url = url.replace("%60","`").replace("%60","`");
            url = url.replace("%7b","{").replace("%7B","{");
            url = url.replace("%7c","|").replace("%7C","|");
            url = url.replace("%7d","}").replace("%7d","}");
            
            return url.replace("%25","%");
        }


    }
}
