/********************************************************************
# Copyright 2021 Daniel 'grindhold' Brendle
#
# This file is part of gas.
#
# gas is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# gas is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with gas.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Gas {
    extern int ldap_authenticate(
        string base_dn, string bind_user, string bind_pw,
        string auth_user, string auth_pw,
        string filter, string server, int port
    );

    public interface Authenticator : GLib.Object {
        public abstract bool authenticate(string username, string password);
    }

    public class MockAuthenticator : GLib.Object, Authenticator {
        public new bool authenticate(string username, string password) {
            message("Mock Authenticator: This is fine… ");
            return true;
        }
    }

    public class LdapAuthenticator : Object, Authenticator {
        public new bool authenticate(string user, string password) {
            try {
                var server = Config.c.get_string("LDAPAuthenticator", "Server");
                var port = Config.c.get_integer("LDAPAuthenticator", "Port");
                var base_dn = Config.c.get_string("LDAPAuthenticator", "BaseDN");
                var bind_user = Config.c.get_string("LDAPAuthenticator", "BindUser");
                var bind_pw = Config.c.get_string("LDAPAuthenticator", "BindPassword");
                var filter = Config.c.get_string("LDAPAuthenticator", "UserFilter");
                return (bool)ldap_authenticate(
                    base_dn,bind_user, bind_pw, user,  password, filter, server, port
                );
            } catch (GLib.KeyFileError e) {
                warning("Could not perform LDAP-Authentication due to missing cfg value" + e.message);
                return false;
            }
        }
    }
}
