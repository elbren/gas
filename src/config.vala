/********************************************************************
# Copyright 2021 Daniel 'grindhold' Brendle
#
# This file is part of gas.
#
# gas is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# gas is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with gas.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Gas {
    public class Config  {
        public static GLib.KeyFile c;

        public static void init() {
            Config.c = new GLib.KeyFile();
            Config.reload();
        }

        public static void reload() {
            try {
                var kf = new GLib.KeyFile();
                kf.load_from_file("/etc/gas/gas.ini", GLib.KeyFileFlags.NONE);
                Config.c = kf;
            } catch (GLib.Error e) {
                warning("Could not reload config. Old config still applies");
            }
        }
    }
    
}
