/********************************************************************
# Copyright 2021 Daniel 'grindhold' Brendle
#
# This file is part of gas.
#
# gas is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# gas is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with gas.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Gas {
    public class Session {
        public string uuid {get; private set;}
        public string username {get; private set;}

        private static int display_cnt = 0; 

        private string generate_uuid(){
            uint8[] buffer = new uint8[16];
            char[] outbuf = new char[36];
            UUID.generate_time_safe(buffer);
            UUID.unparse_lower(buffer,outbuf);
            return (string)outbuf;
        }

        public Session(string username) {
            this.username = username;
            this.uuid = generate_uuid();
            var socket = "/var/run/gas/%s.sock".printf(this.uuid);
            var display = Session.display_cnt++;

            string[] spawn_args = {
                "broadwayd",
                ":%d".printf(display),
                "--unixsocket=%s".printf(socket)
            };

            Array<string> spawn_env = new Array<string>();
            foreach (var e in Environ.get()) {
                spawn_env.append_val(e);
            };
            int broadway_pid, program_pid;

            try {
                Process.spawn_async ("/tmp",
                        spawn_args,
                        spawn_env.data,
                        SpawnFlags.SEARCH_PATH, //| SpawnFlags.DO_NOT_REAP_CHILD,
                        null,
                        out broadway_pid);
            } catch (GLib.SpawnError e) {
                warning("Could not spawn broadway server");
            }

            try {
                spawn_args = Config.c.get_string("ServedApplication", "ExecStart").split(" ");
            } catch (GLib.KeyFileError e) {
                warning("Could not launch application. Check entry 'ExecStart' in config");
            }

            string exec_path;
            try {
                exec_path = Config.c.get_string("ServedApplication", "ExecPath");
            } catch (GLib.KeyFileError e) {
                warning("Could not read ExecPath from config. Running in /tmp");
                exec_path = "/tmp";
            }

            string[] config_env = new string[0];
            try {
                config_env = Config.c.get_string_list("ServedApplication", "Environment");
            } catch (GLib.KeyFileError e) {
                info("Could not find any environment variables for application.");
            }
            spawn_env.append_val("GDK_BACKEND=broadway");
            spawn_env.append_val("BROADWAY_DISPLAY=:%d".printf(display));
            spawn_env.append_val("GAS_USERNAME=%s".printf(username));
            foreach (string env in config_env) {
                spawn_env.append_val(env);
            }

            try {
                Process.spawn_async (exec_path,
                        spawn_args,
                        spawn_env.data,
                        SpawnFlags.SEARCH_PATH, //| SpawnFlags.DO_NOT_REAP_CHILD,
                        null,
                        out program_pid);
            } catch (GLib.SpawnError e) {
                warning("Could not spawn application process");
            }
        }

        public string get_sid() {
            return this.uuid;
        }
    }
}
