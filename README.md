GAS - Gtk Applicationserver
===========================

GAS is a session-management and authentication system for Gtk's Broadway backend.
Think of it as a reverse-proxy for broadway sessions.

The Problem
-----------

Gtk has a feature that's not very widely known. It provides the Gdk-backend broadway
which can render Gtk-Applications on HTML5 Canvases inside browsers. It works great,
you can actually use your application inside the browser. Broadway would be a great
solution to host applications to people that cannot or do not want to install 
software, or for thin client computing.

But broadway itself cannot serve that purpose, because a broadway server merely
accepts one connection from one client at a time. As soon as another client connects
to the server, the former client is immediately being disconnected.

To solve this problem a reverse-proxy-based design was developed to serve arbitrary
sessions simultaneously under a single URL.

The Solution
------------

### Architecture

```
                    tcp                    ipc
 +---------------+                               +---------------+   +---------------+
 |               |                               |   broadwayd   |   |     Gtk-      |
 |    Client     |---+                       +---|               |---|  Application  |
 |               |   |                       |   |       1       |   |      1        |
 +---------------+   |                       |   +---------------+   +---------------+
                     |                       |                                         
 +---------------+   |    +---------------+  |   +---------------+   +---------------+
 |               |   |    |               |  |   |   broadwayd   |   |     Gtk-      |
 |    Client     |---+----|      gas      |--+---|               |---|  Application  |
 |               |   |    |               |  |   |       2       |   |      2        |
 +---------------+   |    +---------------+  |   +---------------+   +---------------+
                     |            |          |                                         
 +---------------+   |            |          |   +---------------+   +---------------+
 |               |   |            |          |   |   broadwayd   |   |     Gtk-      |
 |    Client     |---+            |          +---|               |---|  Application  |
 |               |                |              |       3       |   |      3        |
 +---------------+                |              +---------------+   +---------------+
                                  |
                                  |
                          +---------------+ 
                          |               |
                          | Authentication|
                          |    Backend    |
                          +---------------+

```

### Features

  * [x] Many broadway sessions under one URL
  * [x] LDAP Authentication backend

Installation
------------

### Building from source

#### Installing prerequisites

```
sudo apt install valac meson libldap2-dev uuid-dev
```

#### Building

```
git clone https://codeberg.org/grindhold/gas
cd gas
mkdir build
cd build
meson ..
sudo ninja install
```

### Installing the debian package

```
sudo dpkg -i gas_0.2.0-1_amd64.deb
```

Configuration
-------------

Edit the configuration file `/etc/gas/gas.ini`
See the comments in the file for further info.

Adminstration
-------------

Start and Stop your server with:

```
sudo systemctl start gas
```

and 

```
sudo systemctl stop gas
```

Prior Art
---------

GAS has been inspired by [broadway_proxy](https://github.com/dankasak/broadway_proxy)
by Dan Kasak.

License
-------

![GPLv3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png)
All original work in this repository licensed under GPLv3

The following files are taken from [Gtk](https://gitlab.gnome.org/GNOME/Gtk) and licensed
under LGPLv3:

`data/web/broadway.html`
`data/web/broadway.js`

The following files are taken from [Vala Extra Vapis](https://gitlab.gnome.org/GNOME/vala-extra-vapis)
and licensed under MIT:

`vapi/uuid.deps`
`vapi/uuid.vapi`
